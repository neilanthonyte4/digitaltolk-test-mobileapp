import {StyleSheet, Dimensions} from 'react-native';
import Theme from '../../theme.json';

const useStyle = () => {
    const {width} = Dimensions.get('window');
    const TOASTER_HEIGHT = 50;

    return StyleSheet.create({
        container: (type) => {
            const backgroundColor = type === 'success' ? '#04bf1d' : '#bf0404';

            return {
                width,
                height: TOASTER_HEIGHT,
                backgroundColor
            }
        },
        safe_area: {
            flex: 1,
            backgroundColor: Theme.background
        }
    })
}

export default useStyle;