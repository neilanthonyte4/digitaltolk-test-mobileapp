import React from 'react';
import {View} from 'react-native';
import useStyle from './index.styles';

const Toaster = (props) => {
    const Styles = useStyle();
    const {message, type} = props;
    const container = Styles.container('error');
    
    return (
        <View style={container}>

        </View>
    )
}

export default Toaster;