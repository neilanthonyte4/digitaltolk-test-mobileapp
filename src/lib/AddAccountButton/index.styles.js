import {StyleSheet, Dimensions} from 'react-native';
import Theme from '../../theme.json';

const useStyle = () => {
    const {width} = Dimensions.get('window');
    const BTN_WIDTH = width * 0.15;

    return StyleSheet.create({
        container: {
            justifyContent: 'center',
            alignItems: 'center',
            // position: 'absolute',
            width,
            height: BTN_WIDTH,
            // borderRadius: BTN_WIDTH / 2,
            // bottom: 10,
            // right: 15,
            backgroundColor: Theme.primary,
            flexDirection: 'row'
        },
        add_label: {
            color: '#fff',
            fontWeight: 'bold',
            fontSize: 18
        }
    });
}

export default useStyle;