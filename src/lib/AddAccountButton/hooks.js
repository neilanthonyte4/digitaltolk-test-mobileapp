export const routeToAddNew = (props) => {
    return () => {
        const {navigation} = props;
        navigation.navigate('NEW TODO');
    }
}