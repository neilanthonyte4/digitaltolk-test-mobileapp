import React from "react";
import {TouchableOpacity, Text} from 'react-native';
import useStyle from './index.styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import Theme from '../../theme.json';
import {useNavigation} from '@react-navigation/native';
import * as hooks from './hooks';

const AddAccountButton = () => {
    const Styles = useStyle();
    const navigation = useNavigation();
    const {routeToAddNew} = hooks;

    return (
        <TouchableOpacity style={Styles.container} activeOpacity={0.8} onPress={routeToAddNew({navigation})}>
            {/* <Icon name="plus" size={20} color={Theme.secondary} /> */}
            <Text style={Styles.add_label}>New Todo</Text>
        </TouchableOpacity>
    )
}

export default AddAccountButton;