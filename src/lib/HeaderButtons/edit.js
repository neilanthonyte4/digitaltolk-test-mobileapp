import React from "react";
import {TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import useStyle from './index.styles';
import Theme from '../../theme.json';

const HeaderEditButton = (props) => {
    const Styles = useStyle()['edit'];
    const {navigation} = props;

    const _editStockInfo = (data) => {
        return () => {
            navigation.navigate('NEW LIVESTOCK', {
                action: 'update',
                data
            });
        }
    }

    return (
        <TouchableOpacity activeOpacity={0.7} style={Styles.container} onPress={_editStockInfo(props.route.params.data)}>
            <Icon name="pencil-square-o" size={30} color={Theme.secondary} />
        </TouchableOpacity>
    )
}

export default HeaderEditButton;