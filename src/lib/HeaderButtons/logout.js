import React from "react";
import {TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import useStyle from './index.styles';
import Theme from '../../theme.json';
import * as Backend from '../../db_con';
import {useQueryClient} from 'react-query';
import {CommonActions} from '@react-navigation/native';

const HeaderPrintButton = (props) => {
    const queryClient = useQueryClient();
    const Styles = useStyle()['logout'];
    const {navigation} = props;

    const _onLogout = () => {
        Backend.Props().logout();
        queryClient.setQueriesData(['CURRENT_LOGIN_USER'], null);
        navigation.dispatch(
            CommonActions.reset({
                index: 0,
                routes: [
                    {
                        name: 'LOGIN'
                    },
                ],
            })
        );
    }

    return (
        <TouchableOpacity activeOpacity={0.7} style={Styles.container} onPress={_onLogout}>
            <Icon name="arrow-circle-right" size={30} color={Theme.secondary} />
        </TouchableOpacity>
    )
}

export default HeaderPrintButton;