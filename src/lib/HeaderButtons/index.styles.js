import {StyleSheet} from 'react-native';

const useStyle = () => {
    return {
        logout: StyleSheet.create({
            container: {
                marginRight: 0,
                paddingRight: 5, 
                paddingBottom: 5,
                paddingLeft: 5,
                paddingTop: 5
            }
        })
    }
}

export default useStyle;