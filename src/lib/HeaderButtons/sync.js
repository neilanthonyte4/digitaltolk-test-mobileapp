import React from "react";
import {TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import useStyle from './index.styles';
import Theme from '../../theme.json';
import PouchDB from '../../pouchdb';
// import {openDB} from '../../lib/Database';
import {getState} from 'my-react-global-states';

const HeaderSyncButton = () => {
    const Styles = useStyle()['sync'];
    const db = new PouchDB('pigs.db', { adapter: 'react-native-sqlite' });
    const remotedb = new PouchDB('http://admin:asdasd@192.168.1.10:5984/pigs');

    // function openDB() {
    //   return new PouchDB('pigs.db', { adapter: 'react-native-sqlite' });
    // }
    
    // function openRemoteDB() {
    //   return new PouchDB('http://admin:asdasd@192.168.1.10:5984/pigs');
    // }
    
    const _onSync = async () => {
      try {
        
        if(global.base64FromArrayBuffer) {
          // const db = openDB();
          // console.log('db info:', await db.info());
          // const remotedb = openRemoteDB();
          // console.log('remote db info:', await remotedb.info());
          

          // db.replicate.from(remotedb, {})
          // .on('change', pay_load => {
          //   console.log(" changed!: ");
          // }).on('paused', info => {
          //   console.log("paused");
          // }).on('denied', info => {
          //   console.log("denied");
          // }).on('complete', info => {
          //   console.log("complete");
          // }).on('active', info => {
          //   console.log("Active");
          // }).on('error',  err => {
          //   console.log("error", err);
          // }).catch( err => {
          //   console.log("Error: ",err)
          // });

          db.sync(remotedb, {})
          .on('change', pay_load => {
            console.log(" changed!: ", pay_load);
            if(pay_load && pay_load.change && pay_load.change.docs.length > 0) {
              const changedDocs = pay_load.change.docs;

              console.log('changedDocs', changedDocs);
            }
            getState('refreshStockList')();
          }).on('paused', info => {
            console.log("paused", info);
          }).on('denied', info => {
            console.log("denied", info);
          }).on('complete', info => {
            console.log("complete", info);
          }).on('active', info => {
            console.log("Active", info);
          }).on('error',  err => {
            console.log("error", err);
          }).catch( err => {
            console.log("Error: ",err)
          });
        }
      } catch (error) {
        console.log(error);
      }
    }

    return (
        <TouchableOpacity activeOpacity={0.7} style={Styles.container} onPress={_onSync}>
            <Icon name="rotate-right" size={30} color={Theme.secondary} />
        </TouchableOpacity>
    )
}

export default HeaderSyncButton;