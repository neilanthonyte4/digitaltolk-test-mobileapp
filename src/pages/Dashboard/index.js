import React from 'react';
import {View, ActivityIndicator, Text, TouchableOpacity, Modal, TextInput, AppState} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import useStyle from './index.styles';
import NewTodoBtn from '../../lib/AddAccountButton';
import {SwipeListView} from 'react-native-swipe-list-view';
import TodoItem from './todo_item';
import ItemBtns from './item_btn';
import useHooks from './hooks';
import {useInfiniteQuery, useQueryClient, useQuery} from 'react-query';
import _ from 'underscore';
import {stateListener} from 'my-react-global-states';
import Icon from 'react-native-vector-icons/FontAwesome';
import Theme from '../../theme.json';
import {useAuth} from '../Login/hooks';

const Dashboard = (props) => {
    const {navigation} = props;
    const {reAuth} = useAuth();
    const queryClient = useQueryClient();
    const Styles = useStyle();
    const {fetchTodoIds, updateTodo, checkEndTodo} = useHooks();
    const {data: currentUser} = useQuery(['CURRENT_LOGIN_USER'], reAuth, {staleTime: Infinity, cacheTime: Infinity, retry: false});
    const currentUserId = currentUser ? currentUser._id : null;

    const {isLoading, isError, data, error, isFetching, isRefetching, refetch, fetchNextPage} = useInfiniteQuery(
        ['TODO_IDS', currentUserId], 
        fetchTodoIds, 
        {
            getNextPageParam: (lastPage, pages) => {
                return _.flatten(pages).length;
            },
            staleTime: 0,
            cacheTime: 0
        }
    );
    const list = data ? _.flatten(data.pages) : [];
    const [isProcessing, setIsProcessing] = React.useState(false);
    const [showUpdateLocation, setShowUpdateLocation] = React.useState({show: false, id: null});
    const [current_location, setCurrentLocation] = React.useState(null);
    const [hasError, setError] = React.useState({error: false, message: null});

    stateListener('delete_todo_processing', function(val) {
        setIsProcessing(val);
    })
    stateListener('update_todo_modal', function(val) {
        setShowUpdateLocation(val);
    })
    
    React.useEffect(() => {
        const subscription = AppState.addEventListener("change", async (nextAppState) => {
            if(nextAppState === 'active') {
                await checkEndTodo();
            }
        });
    
        return () => {
          subscription.remove();
        };
    }, []);

    if(isError) {
        return (
            <View style={Styles.no_data_container}>
                <Text style={Styles.error_msg}>{error ? error.message : `Something went wrong while fetching data. Please check your internet connection and try again later.`}</Text>
                <TouchableOpacity style={Styles.try_again_btn} onPress={refetch}>
                    <Text style={Styles.try_again_label}>Try Again</Text>
                </TouchableOpacity>
            </View>
        )
    }
    
    if(isLoading) {
        return (
            <View style={Styles.no_data_container}>
                <ActivityIndicator size="large" color="#325ca8" />
            </View>
        )
    }

    const addNewTodos = () => {
        navigation.navigate('NEW TODO');
    }

    if(!isLoading && list.length < 1) {
        return (
            <View style={Styles.no_data_container}>
                <Text>No Data to be displayed.</Text>
                <TouchableOpacity style={Styles.try_again_btn} onPress={refetch}>
                    <Text style={Styles.try_again_label}>Try Again</Text>
                </TouchableOpacity>
                <TouchableOpacity style={Styles.try_again_btn} onPress={addNewTodos}>
                    <Text style={Styles.try_again_label}>Add new Todo</Text>
                </TouchableOpacity>
            </View>
        )
    }

    const _onUpdateLocation = async (id) => {
        try {
            if(!current_location) {
                setError({
                    hasError: true,
                    message: 'Location can not be empty.'
                });
                return;
            }
            if(!id) {
                setError({
                    hasError: true,
                    message: 'No todo has been selected.'
                });
                return;
            }

            setError({
                hasError: false,
                message: null
            });

            setShowUpdateLocation({
                show: false,
                id: null
            });

            setTimeout(async () => {
                await updateTodo(id, current_location);
                queryClient.invalidateQueries(['TODO_DATA', id]);
                setCurrentLocation(null);
            }, 200)
        } catch (error) {
            throw error;
        }
    }
    
    return (
        <SafeAreaView style={Styles.safe_area} edges={['bottom']}>
            <View style={Styles.container}>

                {isProcessing ? <View style={Styles.loader}>
                    <ActivityIndicator size="large" color='#fff' />
                </View> : null}

                <SwipeListView
                    onRefresh={refetch}
                    refreshing={isRefetching}
                    data={list}
                    leftOpenValue={140}
                    rightOpenValue={-80}
                    renderItem={(item) => <TodoItem {...{...item, ...props}} />}
                    renderHiddenItem={(item) => <ItemBtns {...{...item, ...props}} />}
                    stopLeftSwipe={140}
                    stopRightSwipe={-80}
                    rightActivationValue={-80}
                    rightActionValue={-80}
                    recalculateHiddenLayout={true}
                />

                {
                    isFetching ? (
                        <ActivityIndicator size="large" color="#325ca8" />
                    ) : list.length > 20 ? (
                        <TouchableOpacity style={Styles.load_more_btn} onPress={fetchNextPage}>
                            <Text style={Styles.load_more_label}>load more</Text>
                        </TouchableOpacity>
                    ) : null
                }
                <NewTodoBtn />


                <Modal
                    animationType='fade'
                    visible={showUpdateLocation.show}
                    transparent={true}
                    onRequestClose={() => setShowUpdateLocation(false)}
                >
                    <View style={Styles.update_location_container}>
                        <Text style={Styles.error}>{hasError.message}</Text>
                        <Text style={Styles.note_info}>{`Info: Due to lacking of Google API key I was not able to implement the advance way of getting user current location vie Geolocation API and Google GeocodeInfo API.\n\nHowevever the process is simple, get the location coordinates through Geolocation and use that coordinates to get the location info via Google GeocodeInfo API then update the backend.`}</Text>
                        <Text style={Styles.new_loc_label}>Update Current Location</Text>
                        <View style={Styles.form_container}>
                            <View style={Styles.form_icon_container}>
                                <View style={Styles.form_icon_inner_container}>
                                    <Icon name="map" size={20} color={Theme.secondary} />
                                </View>
                            </View>

                            <TextInput
                                style={Styles.input}
                                placeholder='Set New Current Location'
                                underlineColorAndroid='transparent'
                                autoCapitalize='none'
                                autoCorrect={false}
                                returnKeyType='done'
                                value={current_location}
                                onChangeText={(val) => setCurrentLocation(val)}
                            />
                        </View>

                        <TouchableOpacity style={Styles.save_btn} activeOpacity={0.8} onPress={() => _onUpdateLocation(showUpdateLocation.id)}>
                            {
                                isProcessing ? (
                                    <ActivityIndicator size="small" color={Theme.secondary} />
                                ) : (
                                    <Text style={Styles.save_btn_label}>Update</Text>
                                )
                            }
                        </TouchableOpacity>

                        <TouchableOpacity style={Styles.save_btn} activeOpacity={0.8} onPress={() => {
                            setShowUpdateLocation({show: false, id: null});
                            setError({error: false, message: null});
                        }}>
                            <Text style={Styles.save_btn_label}>Close</Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
            </View>
        </SafeAreaView>
    )
}

export default Dashboard;