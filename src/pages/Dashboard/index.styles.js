import {StyleSheet, Dimensions} from 'react-native';
import Theme from '../../theme.json';

const useStyle = () => {
    const {width, height} = Dimensions.get('window');
    const FORM_CONTAINER_WIDTH = width * 0.95;
    const FOMR_CONTAINER_HEIGHT = 50;

    return StyleSheet.create({
        container: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: Theme.background
        },
        safe_area: {
            flex: 1,
            backgroundColor: Theme.background
        },
        no_data_container: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
        },
        try_again_btn: {
            backgroundColor: Theme.primary,
            padding: 10,
            marginTop: 10,
            borderRadius: 10
        },
        try_again_label: {
            color: '#fff',
        },
        load_more_btn: {
            padding: 10,
        },
        load_more_label: {
            fontWeight: 'bold'
        },
        loader: {
            width,
            height: '100%',
            backgroundColor: 'rgba(0,0,0,0.2)',
            position: 'absolute',
            top: 0,
            left: 0,
            zIndex: 10,
            justifyContent: 'center',
            alignItems: 'center',
        },
        update_location_container: {
            backgroundColor: 'rgba(0,0,0,0.7)',
            flex: 1,
            width,
            justifyContent: 'center',
            alignItems: 'center',
        },
        new_loc_label: {
            width: FORM_CONTAINER_WIDTH * 0.95,
            fontWeight: 'bold',
            color: '#fff',
            fontSize: 16
        },
        form_container: {
            width: FORM_CONTAINER_WIDTH,
            height: FOMR_CONTAINER_HEIGHT,
            borderWidth: 1,
            borderColor: '#fff',
            borderRadius: FORM_CONTAINER_WIDTH / 2,
            flexDirection: 'row',
            overflow: 'hidden',
            marginTop: 10,
            backgroundColor: '#fff'
        },
        form_icon_container: {
            height: FOMR_CONTAINER_HEIGHT,
            width: FOMR_CONTAINER_HEIGHT,
            justifyContent: 'center',
            alignItems: 'center',
            paddingBottom: 2
        },
        form_icon_inner_container: {
            height: FOMR_CONTAINER_HEIGHT * 0.75,
            width: FOMR_CONTAINER_HEIGHT * 0.75,
            backgroundColor: Theme.primary,
            borderRadius: (FOMR_CONTAINER_HEIGHT * 0.75) / 2,
            justifyContent: 'center',
            alignItems: 'center',
        },
        input: {
            flex: 1,
            fontSize: 16
        },
        save_btn: {
            marginTop: 15,
            backgroundColor: Theme.primary,
            width: FORM_CONTAINER_WIDTH,
            height: FOMR_CONTAINER_HEIGHT,
            borderRadius: FORM_CONTAINER_WIDTH / 2,
            justifyContent: 'center',
            alignItems: 'center',
            borderWidth: 1,
            borderColor: '#fff'
        },
        save_btn_label: {
            color: '#fff',
            fontWeight: 'bold',
            fontSize: 18
        },
        error: {
            marginBottom: 10,
            fontWeight: 'bold',
            color: '#ffadad'
        },
        load_more_btn: {
            padding: 10,
        },
        load_more_label: {
            fontWeight: 'bold'
        },
        try_again_btn: {
            backgroundColor: '#2975ff',
            padding: 10,
            marginTop: 10,
            borderRadius: 10
        },
        try_again_label: {
            color: '#fff',
        },
        error_msg: {
            width: width * 0.7,
            textAlign: 'center'
        },
        note_info: {
            textAlign: 'justify',
            width: FORM_CONTAINER_WIDTH * 0.95,
            color: '#fff',
            marginBottom: 10
        }
    })
}

export default useStyle;