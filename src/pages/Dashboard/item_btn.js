import React from 'react';
import {View, TouchableOpacity, Text, ActivityIndicator} from 'react-native';
import useStyle from './item_btn.styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import Theme from '../../theme.json';
import useHooks from './hooks';
import {updateState} from 'my-react-global-states';
import {useQueryClient} from 'react-query';

const ItemBtns = (props) => {
    const Styles = useStyle();
    const queryClient = useQueryClient();
    const {item} = props;
    const {deleteTodo, updateTodo} = useHooks();
    const [isProcessing, setIsProcessing] = React.useState(false);

    const _onPressUpdateBtn = () => {
        updateState('update_todo_modal', {show: true, id: item});
    }

    const _onDeleteLocation = async (id) => {
        try {
            setIsProcessing(true);
            await updateTodo(id, null);
            queryClient.invalidateQueries(['TODO_DATA', id]);
            setIsProcessing(false);
        } catch (error) {
            setIsProcessing(false);
            throw error;
        }
    }

    return (
        <View style={Styles.container}>
            <TouchableOpacity style={[Styles.btns, Styles.del_btn]} activeOpacity={0.85} onPress={() => deleteTodo(item)}>
                <Icon name="trash" size={30} color={Theme.secondary} />
                <Text style={Styles.btn_label}>Delete</Text>
            </TouchableOpacity>
            <TouchableOpacity style={[Styles.btns, Styles.edit_btn]} activeOpacity={0.85} onPress={_onPressUpdateBtn}>
                <Icon name="edit" size={30} color={Theme.secondary} />
                <Text style={Styles.btn_label}>Update</Text>
            </TouchableOpacity>
            <View style={Styles.spacer} />

            {
                isProcessing ? (
                    <ActivityIndicator size="large" color={Theme.secondary} />
                ) : (
                    <TouchableOpacity style={Styles.loading_container} activeOpacity={0.85} onPress={() => _onDeleteLocation(item)}>
                        <Icon name="map" size={30} color={Theme.secondary} />
                        <Text style={Styles.btn_label}>{`Delete\nLocation`}</Text>
                    </TouchableOpacity>
                )
            }
        </View>
    )
}

export default ItemBtns;