import {Alert} from 'react-native';
import * as Backend from '../../db_con';
import _ from 'underscore';
import {useQueryClient} from 'react-query';
import {updateState} from 'my-react-global-states';
import {useAuth} from '../Login/hooks';

export default useHooks = () => {
    const queryClient = useQueryClient();

    return {
        fetchTodoIds: async (props) => {
            try {
                const {queryKey} = props;
                const currentUserId = queryKey[1];

                const todoService = Backend.Service('todos');
                const res = await todoService.find({
                    query: {
                        isDeleted: false,
                        user: currentUserId,
                        $select: ['_id'],
                        $limit: 20,
                    }
                });

                const ids = _.pluck(res.data, '_id');

                return ids;
            } catch (error) {
                throw error;
            }
        },
        fetchTodoData: async (props) => {
            try {
                const {queryKey} = props;
                const id = queryKey[1];
                const todoService = Backend.Service('todos');
                const todoInfo = await todoService.get(id);

                return todoInfo;
            } catch (error) {
                throw error;
            }
        },
        deleteTodo: (id) => {
            try {
                Alert.alert(
                    'Delete Todo',
                    'Are you sure you want to delete it?',
                    [
                        {
                            text: 'Delete',
                            onPress: async () => {
                                updateState('delete_todo_processing', true);
                                const todoService = Backend.Service('todos');
                                const onDelete = await todoService.patch(id, {isDeleted: true});
                                
                                queryClient.invalidateQueries(['TODO_IDS']);
                                updateState('delete_todo_processing', false);
                                return onDelete;
                            }
                        },
                        {
                            text: 'Cancel',
                            onPress: () => {},
                            style: 'cancel'
                        }
                    ]
                )
            } catch (error) {
                updateState('delete_todo_processing', false);
                throw error;
            }
        },
        updateTodo: async (id, newValue) => {
            try {
                updateState('delete_todo_processing', true);
                const todoService = Backend.Service('todos');
                const onUpdate = await todoService.patch(id, {current_location: newValue});

                updateState('delete_todo_processing', false);
                return onUpdate;
            } catch (error) {
                updateState('delete_todo_processing', false);
                throw error;
            }
        },
        startTodo: async (id) => {
            try {
                updateState('delete_todo_processing', true);
                const todoService = Backend.Service('todos');
                const onUpdate = await todoService.patch(id, {startedAt: new Date()});

                updateState('delete_todo_processing', false);
                return onUpdate;
            } catch (error) {
                updateState('delete_todo_processing', false);
                throw error;
            }
        },
        forceDone: async (id) => {
            try {
                updateState('delete_todo_processing', true);
                const todoService = Backend.Service('todos');
                const onUpdate = await todoService.patch(id, {isDone: true});

                updateState('delete_todo_processing', false);
                return onUpdate;
            } catch (error) {
                updateState('delete_todo_processing', false);
                throw error;
            }
        },
        checkEndTodo: async () => {
            try {
                const todoService = Backend.Service('todos');
                const res = await todoService.find({
                    query: {
                        deadline: {
                            $lt : new Date().toISOString()
                        },
                        isDeleted: false,
                        isDone: false,
                        user: currentUserId,
                    }
                });

                const ids = _.pluck(res.data, '_id');

                for(let i = 0; i < ids.length; i++) {
                    queryClient.invalidateQueries(['TODO_DATA', ids[i]]);
                }
            } catch (error) {
                throw error;
            }
        }
    }
}