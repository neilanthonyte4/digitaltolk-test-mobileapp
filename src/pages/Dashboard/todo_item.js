import React from 'react';
import {View, Text, TouchableOpacity, Alert, Platform} from 'react-native';
import useStyle from './todo_item.styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import {useQuery, useQueryClient} from 'react-query';
import useHooks from './hooks';
import Pie from 'react-native-pie';
import moment from 'moment';
import * as twix from 'twix';

const TodoItem = (props) => {
    const queryClient = useQueryClient();
    const Styles = useStyle();
    const {item} = props;
    const getPercent = 50;
    const progress = Styles.progress(getPercent);
    const {fetchTodoData, startTodo, forceDone} = useHooks();
    const {data: itemInfo} = useQuery(['TODO_DATA', item], fetchTodoData, {staleTime: 0, cacheTime: 0});

    const data = {
        startedAt: itemInfo ? itemInfo.startedAt : null, 
        deadline: itemInfo ? itemInfo.deadline : null, 
        name: itemInfo ? itemInfo.name : null, 
        current_location: itemInfo ? itemInfo.current_location : null, 
        description: itemInfo ? itemInfo.description : null, 
        isDone: itemInfo ? itemInfo.isDone : null
    };

    const renderName = data.name === null ? '' : data.name && data.name.length > 25 ? data.name.substring(0,25)+'...' : data.name;
    const renderLocation = data.current_location === null ? '' : data.current_location && data.current_location.length > 25 ? data.current_location.substring(0,25)+'...' : data.current_location;
    const renderDesc = data.description === null ? '' : data.description && data.description.length > 40 ? data.description.substring(0,40)+'...' : data.description;

    const _setDone = async (id) => {
        try {
            await forceDone(id);
            queryClient.invalidateQueries(['TODO_DATA', id]);
        } catch (error) {
            throw error;
        }
    }

    const getDateProgress = (start, end) => {
        try {
            if(start && end) {
                const now = new Date().getTime();
                const startTime = new Date(start).getTime();
                const endTime = new Date(end).getTime();
                const t = moment(startTime).twix(now);
                const t2 = moment(startTime).twix(endTime);
                const progress = (t.count('minutes') / t2.count('minutes')) * 100;

                return progress <= 100 ? Math.floor(progress) : 100;
            }

            return 0;
        } catch (error) {
            throw error;
        }
    }

    const taskPercentage =  getDateProgress(data.startedAt, data.deadline);

    React.useEffect(() => {
        const now = new Date().getTime();
        const endTime = new Date(data.deadline).getTime();

        if(taskPercentage >= 100 && data.isDone !== null && data.isDone === false) {
            _setDone(item);
        }
    },[data.deadline, data.isDone, taskPercentage])

    
    const _startTodo = async () => {
        try {
            await startTodo(item);
            queryClient.invalidateQueries(['TODO_DATA', item]);
        } catch (error) {
            throw error;
        }
    }

    const _onForceDone = (done, startedAt) => {
        if(done) return;

        if(!startedAt) return;

        Alert.alert(
            'Finish Task',
            `Are you sure you are done?\n\n${Platform.OS === 'android' ? '' : 'Note: Due to lacking of Apple push certificate and provisioning profile, this version will not receive push notification when todo completes.'}`,
            [
                {
                    text: 'Yes',
                    onPress: () => {
                        try {
                            setTimeout(async() => {
                                await forceDone(item);
                                queryClient.invalidateQueries(['TODO_DATA', item]);
                            }, 200)
                        } catch (error) {
                            throw error;
                        }
                    }
                },
                {
                    text: 'Cancel',
                    onPress: () => {},
                    style: 'cancel'
                }
            ]
        )
    }
    
    return (
        <TouchableOpacity style={Styles.container} activeOpacity={1} onPress={() => _onForceDone(data.isDone, data.startedAt)}>
            <View style={Styles.inner_container}>

                <View style={Styles.todo_info_wrapper}>
                    <Text style={Styles.todo_name}>{renderName}</Text>
                    {
                        data.isDone === null ? null : data.isDone ? (
                            <Text style={Styles.complete}>Complete</Text>
                        ) : (
                            <Text style={Styles.created_at}>Deadline: {data.deadline ? moment(data.deadline).fromNow() : null}</Text>
                        )
                    }
                    <Text>{renderDesc}</Text>
                    <Text><Text style={Styles.loc_label}>Location:</Text> {renderLocation ? renderLocation : '-not set-'}</Text>
                </View>

                {
                    typeof itemInfo === 'undefined' ? null : data.startedAt ? (
                        <View style={Styles.start_btn_progress_container}>
                            <Pie
                                radius={35}
                                innerRadius={30}
                                sections={[
                                {
                                    percentage: data.isDone ? 100 : taskPercentage,
                                    color: 'green',
                                },
                                ]}
                                backgroundColor="#ddd"
                            />
                            <Text style={Styles.percent_label}>{data.isDone ? 'Done' : `${taskPercentage}%`}</Text>
                        </View>
                    ) : (
                        <View style={Styles.start_btn_progress_container}>
                            <TouchableOpacity style={Styles.start_btn} activeOpacity={0.75} onPress={_startTodo}>
                                <Text style={Styles.start_btn_label}>{`Start\nTodo`}</Text>
                            </TouchableOpacity>
                        </View>
                    )
                }

            </View>
        </TouchableOpacity>
    )
}

export default TodoItem;