import {StyleSheet, Dimensions} from 'react-native';
import Theme from '../../theme.json';

const useStyle = () => {
    const {width} = Dimensions.get('window');
    const ITEM_WIDTH = width;
    const ITEM_HEIGHT = 100;
    const INNER_ITEM_WIDTH = ITEM_WIDTH;
    const INNER_ITEM_HEIGHT = ITEM_HEIGHT;

    return StyleSheet.create({
        container: {
            width: ITEM_WIDTH,
            height: ITEM_HEIGHT,
            backgroundColor: '#fff',
            borderBottomWidth: 1,
            borderBottomColor: '#d4d5d6',
            justifyContent: 'center',
            alignItems: 'center',
            overflow: 'hidden',
        },
        inner_container: {
            width: INNER_ITEM_WIDTH,
            height: INNER_ITEM_HEIGHT,
            overflow: 'hidden',
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'row'
        },
        todo_info_wrapper: {
            flex: 1,
            height: INNER_ITEM_HEIGHT,
            paddingTop: 10,
            paddingLeft: 10,
            paddingBottom: 10
        },
        todo_progress: {
            width: INNER_ITEM_WIDTH,
            height: INNER_ITEM_HEIGHT * 0.15,
        },
        todo_name: {
            fontWeight: 'bold',
            fontSize: 18
        },
        location: {
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            marginTop: 5,
            marginBottom: 5,
            maxWidth: INNER_ITEM_WIDTH
        },
        created_at: {
            fontSize: 12,
            color: '#e0a500',
            fontWeight: 'bold'
        },
        progress: (progress) => {
            const percent = progress / 100;

            return {
                width: INNER_ITEM_WIDTH * percent,
                height: INNER_ITEM_HEIGHT * 0.15,
                backgroundColor: '#0fd931',
                borderRadius: INNER_ITEM_WIDTH / 2
            }
        },
        start_btn_progress_container: {
            height: INNER_ITEM_HEIGHT,
            width: INNER_ITEM_HEIGHT,
            justifyContent: 'center',
            alignItems: 'center',
        },
        percent_label: {
            position: 'absolute',
            zIndex: 5
        },
        start_btn: {
            height: INNER_ITEM_HEIGHT * 0.65,
            width: INNER_ITEM_HEIGHT * 0.65,
            backgroundColor: '#edad4c',
            borderRadius: (INNER_ITEM_HEIGHT * 0.65) / 2,
            justifyContent: 'center',
            alignItems: 'center',
        },
        start_btn_label: {
            color: '#fff',
            fontWeight: 'bold'
        },
        loc_label: {
            fontWeight: 'bold', 
            color: Theme.primary
        },
        complete: {
            fontSize: 12,
            color: 'green',
            fontWeight: 'bold'
        }
    })
}

export default useStyle;