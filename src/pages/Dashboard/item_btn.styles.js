import {StyleSheet, Dimensions} from 'react-native';
import Theme from '../../theme.json';

const useStyle = () => {
    const {width} = Dimensions.get('window');
    const ITEM_WIDTH = width;
    const BTN_WIDTH = 70;
    const ITEM_HEIGHT = 100;

    return StyleSheet.create({
        container: {
            width: ITEM_WIDTH,
            height: ITEM_HEIGHT,
            flexDirection: 'row',
            backgroundColor: '#006bc2'
        },
        btns: {
            width: BTN_WIDTH,
            justifyContent: 'center',
            alignItems: 'center'
        },
        del_btn: {
            // backgroundColor: '#c20000',
        },
        edit_btn: {
            // backgroundColor: Theme.primary,
        },
        btn_label: {
            textAlign: 'center',
            fontWeight: 'bold',
            color: Theme.secondary
        },
        loading_container: {
            justifyContent: 'center',
            alignItems: 'center',
            marginRight: 10
        },
        spacer: {
            flex: 1
        }
    })
}

export default useStyle;