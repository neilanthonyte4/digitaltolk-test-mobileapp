import * as Backend from '../../db_con';

export const useAuth = () => {
    return {
        loginValidation: (email, password) => {
            return new Promise((resolve, reject) => {
                if(!email || !password) {
                    reject({message: 'Email or Password must not be empty.'});
                }

                const regex = RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
                const checkforat = email.split('@');

                if(checkforat.length < 2) {
                    reject({message: 'Email format is invalid.'});
                }

                const checkfordot = checkforat[1].split('.');
                if(checkforat.length < 3 && checkfordot.length < 4 && regex.test(email) && /\s/.test(email) === false) {
                    
                } else {
                    reject({message: 'Email format is invalid.'});
                }

                if(password.length < 6) {
                    reject({message: 'Password must be greater than 6 characters.'})
                }

                resolve('success');
            });
        },
        userLogin: async (credentials) => {
            try {
                const userService = Backend.Service('users');
                const response = await Backend.Props().authenticate(credentials);
                const userInfo = await userService.get(response.user._id);
    
                return userInfo;
            } catch (error) {
                throw new Error('Email or Password is incorrect.')
            }
        },
        reAuth: async () => {
            try {
                const userService = Backend.Service('users');
                const response = await Backend.Props().authenticate();
                const loginUserInfo = await userService.get(response.user._id);

                return loginUserInfo;
            } catch (error) {
                throw error.message;
            }
        }
    }
}