import {StyleSheet, Dimensions} from 'react-native';
import Theme from '../../theme.json';

const useStyle = () => {
    const {width} = Dimensions.get('window');
    const FIELD_WIDTH = width * 0.85;
    const FIELD_HEIGHT = 50;
    const TEXT_INPUT_WIDTH = FIELD_WIDTH - FIELD_HEIGHT;

    return StyleSheet.create({
        container: {
            backgroundColor: Theme.primary,
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
        },
        field_wrapper: {
            width: FIELD_WIDTH,
            height: FIELD_HEIGHT,
            backgroundColor: '#fff',
            borderRadius: FIELD_WIDTH / 2,
            flexDirection: 'row',
            overflow: 'hidden'
        },
        spacer: {
            height: 15
        },
        icon_container: {
            width: FIELD_HEIGHT,
            height: FIELD_HEIGHT,
            justifyContent: 'center',
            alignItems: 'flex-end'
        },
        text_input_container: {
            flex: 1,
            padding: 5,
            justifyContent: 'center',
            alignItems: 'center',
            overflow: 'hidden'
        },
        text_input: {
            flex: 1,
            width: TEXT_INPUT_WIDTH * 0.95,
        },
        login_btn: {
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: Theme.dark_primary,
            width: FIELD_WIDTH,
            height: FIELD_HEIGHT,
            borderRadius: FIELD_WIDTH / 2,
            borderWidth: 1,
            borderColor: Theme.secondary
        },
        login_btn_label: {
            fontWeight: 'bold',
            fontSize: 20,
            color: Theme.secondary
        },
        keyboard_avoiding: {
            flex: 1
        },
        error_message: {
            marginBottom: 5,
            color: '#f3ff00',
            fontSize: 14,
            fontWeight: 'bold'
        }
    })
}

export default useStyle;