import React from 'react';
import {View, TextInput, Platform, TouchableOpacity, Text, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard, ActivityIndicator} from 'react-native';
import useStyle from './index.styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import Theme from '../../theme.json';
import {CommonActions} from '@react-navigation/native';
import {useAuth} from './hooks';
import {useQuery, useQueryClient} from 'react-query';

import OneSignal from 'react-native-onesignal';

OneSignal.setLogLevel(6, 0);
OneSignal.setAppId("7bf8e8ad-018c-4708-979c-efc15fddede9");

const Login = (props) => {
    const Styles = useStyle();
    const queryClient = useQueryClient();
    const passRef = React.useRef();
    const {navigation} = props;
    const {userLogin, loginValidation, reAuth} = useAuth();
    const [email, setEmail] = React.useState(null);
    const [password, setPassword] = React.useState(null);
    const [loginError, setLoginError] = React.useState({hasError: false, message: null})
    const {data, isLoading} = useQuery(['CURRENT_LOGIN_USER'], reAuth, {staleTime: Infinity, cacheTime: Infinity, retry: false});
    const [processing, setProcessing] = React.useState(false);

    const _onSubmit = (type, ref) => {
        return async () => {
            try {
                if(type === 'next') {
                    ref.current.focus();
                } else {
                    setProcessing(true);
                    const res = await loginValidation(email, password);
                    
                    if(res === 'success') {
                        const credentials = {
                            strategy: 'local',
                            email: email.trim(),
                            password: password.trim(),
                        };
                        
                        await userLogin(credentials);
                        queryClient.invalidateQueries(['CURRENT_LOGIN_USER']);
                        setProcessing(false);
                    }
                }
            } catch (error) {
                setProcessing(false);
                setLoginError({
                    hasError: true,
                    message: error.message
                });
            }
        }
    }

    const _onChangeEmail = (val) => {
        setEmail(val);

        if(loginError.hasError) {
            setLoginError({
                hasError: false,
                message: null
            });
        }
    }

    const _onChangePassword = (val) => {
        setPassword(val);

        if(loginError.hasError) {
            setLoginError({
                hasError: false,
                message: null
            });
        }
    }

    if(isLoading) {
        return (
            <View style={Styles.container}>
                <ActivityIndicator size="large" color={Theme.secondary} />
            </View>
        )
    }
    
    if(!isLoading && !data) {
        return (
            <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : "height"}
                style={Styles.keyboard_avoiding}
            >
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <View style={Styles.container}>
    
                        {
                            loginError.hasError ? 
                                (<Text style={Styles.error_message}>{loginError.message}</Text>)
                            : null
                        }
    
                        <View style={Styles.field_wrapper}>
                            <View style={Styles.icon_container}>
                                <Icon name="envelope" size={25} color={Theme.primary} />
                            </View>
                            <View style={Styles.text_input_container}>
                                <TextInput 
                                    autoCapitalize='none'
                                    autoComplete='email'
                                    autoCorrect={false}
                                    keyboardType='email-address'
                                    placeholder='enter email'
                                    returnKeyType='next'
                                    onChangeText={_onChangeEmail}
                                    onSubmitEditing={_onSubmit('next', passRef)}
                                    style={Styles.text_input} />
                            </View>
                        </View>
    
                        <View style={Styles.spacer} />
    
                        <View style={Styles.field_wrapper}>
                            <View style={Styles.icon_container}>
                                <Icon name="key" size={25} color={Theme.primary} />
                            </View>
                            <View style={Styles.text_input_container}>
                                <TextInput 
                                    ref={passRef}
                                    autoCapitalize='none'
                                    placeholder='enter password'
                                    secureTextEntry={true}
                                    returnKeyType='done'
                                    onChangeText={_onChangePassword}
                                    onSubmitEditing={_onSubmit('done')}
                                    style={Styles.text_input} />
                            </View>
                        </View>
    
                        <View style={Styles.spacer} />
    
                        <TouchableOpacity style={Styles.login_btn} onPress={_onSubmit('done')}>
                            {
                                processing ?  <ActivityIndicator size="small" color={Theme.secondary} /> : <Text style={Styles.login_btn_label}>Login</Text>}
                        </TouchableOpacity>
                    </View>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        )
    }

    return (
        <View onLayout={() => {
            navigation.dispatch(
                CommonActions.reset({
                    index: 0,
                    routes: [
                        {
                            name: 'MY TODOS'
                        },
                    ],
                })
            );
        }}>

        </View>
    )
}

export default Login;