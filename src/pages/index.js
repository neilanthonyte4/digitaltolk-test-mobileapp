import Dashboard from "./Dashboard";
import Login from "./Login";
import CreateAccount from './CreateAccount';
import NewTodo from "./NewTodo";

export default {
    Dashboard,
    Login,
    CreateAccount,
    NewTodo
}