import {StyleSheet} from 'react-native';
import Theme from '../../theme.json';

const useStyle = () => {
    return StyleSheet.create({
        container: {},
        safe_area: {
            flex: 1,
            backgroundColor: Theme.background
        }
    })
}

export default useStyle;