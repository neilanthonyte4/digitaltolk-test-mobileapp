import React from 'react';
import {View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import useStyle from './index.styles';

const CreateAccount = () => {
    const Styles = useStyle();
    
    return (
        <SafeAreaView style={Styles.safe_area} edges={['bottom']}>
            <View style={Styles.container}>

            </View>
        </SafeAreaView>
    )
}

export default CreateAccount;