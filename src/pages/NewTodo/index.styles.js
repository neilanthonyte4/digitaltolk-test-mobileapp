import {StyleSheet, Dimensions} from 'react-native';
import Theme from '../../theme.json';

const useStyle = () => {
    const {width} = Dimensions.get('window');
    const FORM_CONTAINER_WIDTH = width * 0.95;
    const FOMR_CONTAINER_HEIGHT = 50;

    return StyleSheet.create({
        container: {
            flex: 1,
            width,
            justifyContent: 'flex-start',
            alignItems: 'center',
            paddingTop: 10,
            backgroundColor: '#fff'
        },
        outer_container: {
            flex: 1,
            justifyContent: 'flex-start',
            alignItems: 'center',
        },
        safe_area: {
            flex: 1,
            backgroundColor: Theme.secondary
        },
        field_label: {
            fontWeight: 'bold',
        },
        form_container: {
            width: FORM_CONTAINER_WIDTH,
            height: FOMR_CONTAINER_HEIGHT,
            borderWidth: 1,
            borderRadius: FORM_CONTAINER_WIDTH / 2,
            flexDirection: 'row',
            overflow: 'hidden',
            marginTop: 10
        },
        form_icon_container: {
            height: FOMR_CONTAINER_HEIGHT,
            width: FOMR_CONTAINER_HEIGHT,
            justifyContent: 'center',
            alignItems: 'center',
            paddingBottom: 2
        },
        form_icon_inner_container: {
            height: FOMR_CONTAINER_HEIGHT * 0.75,
            width: FOMR_CONTAINER_HEIGHT * 0.75,
            backgroundColor: Theme.primary,
            borderRadius: (FOMR_CONTAINER_HEIGHT * 0.75) / 2,
            justifyContent: 'center',
            alignItems: 'center',
        },
        bullet_form_container: {
            width: FORM_CONTAINER_WIDTH * 0.55,
            height: FOMR_CONTAINER_HEIGHT,
            borderWidth: 1,
            borderRadius: FORM_CONTAINER_WIDTH / 2,
            flexDirection: 'row',
            overflow: 'hidden',
            marginTop: 10
        },
        bullet_button: {
            height: FOMR_CONTAINER_HEIGHT * 0.75,
            width: FOMR_CONTAINER_HEIGHT * 0.75,
            borderColor: Theme.primary,
            borderWidth: 3,
            borderRadius: (FOMR_CONTAINER_HEIGHT * 0.75) / 2,
            justifyContent: 'center',
            alignItems: 'center',
        },
        input: {
            flex: 1,
            fontSize: 16
        },
        save_btn: {
            marginTop: 15,
            backgroundColor: Theme.primary,
            width: FORM_CONTAINER_WIDTH,
            height: FOMR_CONTAINER_HEIGHT,
            borderRadius: FORM_CONTAINER_WIDTH / 2,
            justifyContent: 'center',
            alignItems: 'center',
        },
        save_btn_label: {
            color: '#fff',
            fontWeight: 'bold',
            fontSize: 18
        },
        status: (type) => {
            const color = type === 'success' ? '#04bf1d' : '#bf0404';
            return {
                textAlign: 'center',
                color,
                fontWeight: 'bold'
            }
        },
        set_deadline: {
            justifyContent: 'center',
            alignItems: 'flex-start',
            flex: 1,
        },
        set_deadline_label: (hasDeadline) => {
            return {
                color: hasDeadline ? '#000' : '#b5b5b5',
                fontSize: 16,
            }
        },
        time_input: {
            width: FORM_CONTAINER_WIDTH * 0.25,
        },
        bullet_form_container_time: {
            width: FORM_CONTAINER_WIDTH * 0.42,
            height: FOMR_CONTAINER_HEIGHT,
            borderWidth: 1,
            borderRadius: FORM_CONTAINER_WIDTH / 2,
            flexDirection: 'row',
            overflow: 'hidden',
            marginTop: 10,
            marginRight: 10
        },
        android_select_time_btn: {
            width: FORM_CONTAINER_WIDTH * 0.27,
            height: FOMR_CONTAINER_HEIGHT,
            justifyContent: 'center',
        },
        selected_time_android_label: {
            fontSize: 16
        }
    })
}

export default useStyle;