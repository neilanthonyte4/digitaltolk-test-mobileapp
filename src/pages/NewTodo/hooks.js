import React from 'react';
import * as Backend from '../../db_con';

export default useHooks = () => {
    const [formData, setFormData] = React.useState({
        name: null,
        description: null,
        deadline: null,
        createdAt: new Date(),
        started: null,
        current_location: null
    });
    const [isProcessing, setProcessing] = React.useState(false);
    const [saveStatus, setSaveStatus] = React.useState({
        type: null,
        message: null
    });

    async function saveTodo() {
        if(isProcessing) return;

        setProcessing(true);

        try {
            const todoService = Backend.Service('todos');
            const onCreateTodo = await todoService.create(formData);

            setFormData({
                name: null,
                description: null,
                deadline: null,
                createdAt: new Date(),
                started: null,
                current_location: null
            });
            setProcessing(false);
            setSaveStatus({
                type: 'success',
                message: 'Task successfully created.'
            });
            
            return onCreateTodo;
        } catch (error) {
            setProcessing(false);
            setSaveStatus({
                type: 'error',
                message: `Something is wrong while saving your record.\nPlease check your connection and try again later.`
            });
            throw 'error.message';
        }
    }

    function onInputChange(formField, value) {
        formData[formField] = value;
        setFormData(formData);

        if(saveStatus.type) {
            setSaveStatus({
                type: null,
                 message: null
            });
        }
    }

    return {saveTodo, onInputChange, formData, isProcessing, saveStatus}
}