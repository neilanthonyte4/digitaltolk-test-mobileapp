import React from 'react';
import {View, Text, Platform, TouchableOpacity, ActivityIndicator, TextInput, ScrollView, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import useStyle from './index.styles';
import Icon from 'react-native-vector-icons/FontAwesome';
import Theme from '../../theme.json';
import * as Backend from '../../db_con';
import {Calendar} from 'react-native-calendars';
import moment from 'moment';
import {useQueryClient, useQuery} from 'react-query';
import DateTimePicker from '@react-native-community/datetimepicker';
import {useAuth} from '../Login/hooks';
import OneSignal from 'react-native-onesignal';

const NewTodo = () => {
    const queryClient = useQueryClient();
    const {reAuth} = useAuth();
    const {data} = useQuery(['CURRENT_LOGIN_USER'], reAuth, {staleTime: Infinity, cacheTime: Infinity, retry: false});
    const currentUserId = data ? data._id : null;

    const Styles = useStyle();
    const [name, setName] = React.useState(null);
    const [description, setDescription] = React.useState(null);
    const [deadline, setDeadline] = React.useState(null);
    const [showCalendar, setShowCalendar] = React.useState(false);
    const [current_location, setCurrentLocation] = React.useState(null);
    const [startedAt, setStartedAt] = React.useState(new Date());
    const [showAndroidTime, setShowAndroidTime] = React.useState(false);

    const descriptionRef = React.useRef();
    const locationRef = React.useRef();

    const [isProcessing, setProcessing] = React.useState(false);
    const [saveStatus, setSaveStatus] = React.useState({
        type: null,
        message: null
    });
    const formData = {
        name,
        description,
        deadline,
        createdAt: new Date(),
        startedAt,
        current_location,
        isDeleted: false,
        isDone: false,
        device: Platform.OS,
        user: currentUserId
    };

    function validateForm(frm) {
        return new Promise((resolve, reject) => {
            const {name, description, current_location, deadline} = frm;

            if(name && name.length < 1 || !name) {
                reject({message: 'Name must not be empty', type: 'validation'});
            }

            if(description && description.length < 1 || !description) {
                reject({message: 'Description must not be empty', type: 'validation'});
            }

            if(current_location && current_location.length < 1 || !current_location) {
                reject({message: 'Current location must not be empty', type: 'validation'});
            }

            if(name && name.length < 2) {
                reject({message: 'Name must be greater than 2 characters.', type: 'validation'});
            }

            if(description && description.length < 2) {
                reject({message: 'Description description must be greater than 2 characters.', type: 'validation'});
            }

            if(current_location && current_location.length < 2) {
                reject({message: 'Current must be greater than 2 characters', type: 'validation'});
            }

            if(!deadline) {
                reject({message: 'Please set a deadline.', type: 'validation'});
            }

            resolve('success');
        })
    }

    async function saveTodo(formData) {
        if(isProcessing) return;

        setProcessing(true);

        try {
            const {userId: pushToken} = await OneSignal.getDeviceState();
            await validateForm(formData);
            
            const todoService = Backend.Service('todos');
            const onCreateTodo = await todoService.create({...formData, pushToken});

            setName(null);
            setDescription(null);
            setDeadline(null);
            setCurrentLocation(null);
            setProcessing(false);
            setSaveStatus({
                type: 'success',
                message: 'Task successfully created.'
            });

            queryClient.invalidateQueries(['TODO_IDS']);
            
            return onCreateTodo;
        } catch (error) {
            setProcessing(false);
            let message = `Something is wrong while saving your record.\nPlease check your connection and try again later.`;

            if(error && error.type && error.type === 'validation') {
                message = error.message;
            }
            
            setSaveStatus({
                type: 'error',
                message
            });
            throw error.message;
        }
    }

    function _onNameChange(val) {
        setName(val);
        if(saveStatus.type) {
            setSaveStatus({
                type: null,
                 message: null
            });
        }
    }

    function _onDescriptionChange(val) {
        setDescription(val);
        if(saveStatus.type) {
            setSaveStatus({
                type: null,
                 message: null
            });
        }
    }

    function _onDeadlineChange(day) {
        setDeadline(new Date(day.dateString));
        setShowCalendar(false)
        if(saveStatus.type) {
            setSaveStatus({
                type: null,
                 message: null
            });
        }
    }

    function _onCurrentLocationChange(val) {
        setCurrentLocation(val);
        if(saveStatus.type) {
            setSaveStatus({
                type: null,
                 message: null
            });
        }
    }

    function _onStartImmediately() {
        if(!startedAt) {
            setStartedAt(new Date());
        } else {
            setStartedAt(null);
        }
    }

    const _onSelectTime = (event, selectedDate) => {
        if(Platform.OS === 'android' && !selectedDate) {
            setShowAndroidTime(false);
        } else {
            setShowAndroidTime(false);
            setDeadline(selectedDate)
        }
    }

    const _onOpenTimerPickerAndroid = () => {
        setShowAndroidTime(true);
    }
    
    return (
        <SafeAreaView style={Styles.safe_area} edges={['bottom']}>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View style={Styles.outer_container}>
                    <View style={Styles.container}>
                        <Text style={Styles.status(saveStatus.type)}>{saveStatus.message}</Text>
                        <KeyboardAvoidingView
                            behavior={Platform.OS === "ios" ? "padding" : "height"}
                            style={Styles.keyboard_avoiding}
                            keyboardVerticalOffset={100}
                        >
                            <ScrollView>
                                <View style={Styles.form_container}>
                                    <View style={Styles.form_icon_container}>
                                        <View style={Styles.form_icon_inner_container}>
                                            <Icon name="list-ul" size={20} color={Theme.secondary} />
                                        </View>
                                    </View>

                                    <TextInput
                                        style={Styles.input}
                                        placeholder='Enter todo name'
                                        underlineColorAndroid='transparent'
                                        autoCapitalize='none'
                                        autoCorrect={false}
                                        returnKeyType='next'
                                        value={name}
                                        onChangeText={(val) => _onNameChange(val)}
                                        onSubmitEditing={() => {
                                            descriptionRef.current.focus();
                                        }}
                                    />
                                </View>

                                <View style={Styles.form_container}>
                                    <View style={Styles.form_icon_container}>
                                        <View style={Styles.form_icon_inner_container}>
                                            <Icon name="info-circle" size={20} color={Theme.secondary} />
                                        </View>
                                    </View>

                                    <TextInput
                                        ref={descriptionRef}
                                        style={Styles.input}
                                        placeholder='Enter description'
                                        underlineColorAndroid='transparent'
                                        autoCapitalize='none'
                                        autoCorrect={false}
                                        returnKeyType='next'
                                        value={description}
                                        onChangeText={(val) => _onDescriptionChange(val)}
                                        onSubmitEditing={() => {
                                            locationRef.current.focus();
                                        }}
                                    />
                                </View>

                                <View style={Styles.form_container}>
                                    <View style={Styles.form_icon_container}>
                                        <View style={Styles.form_icon_inner_container}>
                                            <Icon name="calendar" size={20} color={Theme.secondary} />
                                        </View>
                                    </View>
                                    <TouchableOpacity style={Styles.set_deadline} onPress={() => setShowCalendar(!showCalendar)}>
                                        <Text style={Styles.set_deadline_label(deadline)}>{`${deadline ? moment(deadline).format('MMMM D YYYY') : 'Set Deadline Date'}`}</Text>
                                    </TouchableOpacity>
                                </View>

                                {
                                    showCalendar ? (
                                        <Calendar
                                            minDate={moment(new Date()).format('YYYY-MM-DD')}
                                            enableSwipeMonths={true}
                                            onDayPress={_onDeadlineChange}
                                        />
                                    ) : null
                                }

                                <View style={{flexDirection: 'row'}}>
                                    <View style={Styles.bullet_form_container_time}>
                                        <View style={Styles.form_icon_container}>
                                            <View style={Styles.form_icon_inner_container}>
                                                <Icon name="clock-o" size={20} color={Theme.secondary} />
                                            </View>
                                        </View>
                                        <View style={Styles.set_deadline}>
                                            {
                                                Platform.OS === 'ios' ? (
                                                    <DateTimePicker
                                                        value={deadline ? new Date(deadline) : new Date()}
                                                        mode={'time'}
                                                        is24Hour={true}
                                                        display="compact"
                                                        style={Styles.time_input}
                                                        onChange={_onSelectTime}
                                                    />
                                                ) : (
                                                    <TouchableOpacity style={Styles.android_select_time_btn} activeOpacity={0.75} onPress={_onOpenTimerPickerAndroid}>
                                                        <Text style={Styles.selected_time_android_label}>{deadline ? moment(new Date(deadline)).format('h:mm: a').toUpperCase() : moment(new Date()).format('h:mm: a').toUpperCase()}</Text>
                                                    </TouchableOpacity>
                                                )
                                            }

                                            {
                                                showAndroidTime ? (
                                                    <DateTimePicker 
                                                        value={deadline ? new Date(deadline) : new Date()}
                                                        mode={'time'}
                                                        is24Hour={true}
                                                        display="default"
                                                        style={Styles.time_input}
                                                        onChange={_onSelectTime}
                                                        neutralButtonLabel="clear" />
                                                ) : null
                                            }
                                        </View>
                                    </View>

                                    <TouchableOpacity style={Styles.bullet_form_container} activeOpacity={0.7} onPress={_onStartImmediately}>
                                        <View style={Styles.form_icon_container}>
                                            <View style={Styles.bullet_button}>
                                                {
                                                    startedAt ? (
                                                        <Icon name="check" size={20} color={Theme.primary} />
                                                    ) : null
                                                }
                                            </View>
                                        </View>

                                        <View style={Styles.set_deadline} onPress={() => setShowCalendar(!showCalendar)}>
                                            <Text style={Styles.set_deadline_label(true)}>Start Immediately</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>

                                <View style={Styles.form_container}>
                                    <View style={Styles.form_icon_container}>
                                        <View style={Styles.form_icon_inner_container}>
                                            <Icon name="map" size={20} color={Theme.secondary} />
                                        </View>
                                    </View>

                                    <TextInput
                                        ref={locationRef}
                                        style={Styles.input}
                                        placeholder='Set Current Location'
                                        underlineColorAndroid='transparent'
                                        autoCapitalize='none'
                                        autoCorrect={false}
                                        returnKeyType='done'
                                        value={current_location}
                                        onChangeText={(val) => _onCurrentLocationChange(val)}
                                    />
                                </View>

                                <TouchableOpacity style={Styles.save_btn} activeOpacity={0.8} onPress={() => saveTodo(formData)}>
                                    {
                                        isProcessing ? (
                                            <ActivityIndicator size="small" color={Theme.secondary} />
                                        ) : (
                                            <Text style={Styles.save_btn_label}>Save</Text>
                                        )
                                    }
                                </TouchableOpacity>
                            </ScrollView>
                        </KeyboardAvoidingView>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        </SafeAreaView>
    )
}

export default NewTodo;