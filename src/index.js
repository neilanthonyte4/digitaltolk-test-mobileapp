import React from "react";
import App from './app';
import {QueryClient, QueryClientProvider} from 'react-query';

const queryClient = new QueryClient();

const DigitalTolk = () => {
    return (
        <QueryClientProvider client={queryClient}>
            <App />
        </QueryClientProvider>
    )
}

export default DigitalTolk;