import React from 'react';
import {View, StyleSheet} from 'react-native';
import Navs from './navs';

const Styles = StyleSheet.create({
    container: {
        flex: 1
    }
})

const App = () => {
    return (
        <View style={Styles.container}>
            <Navs />
        </View>
    )
}

export default App;