import {initState} from 'my-react-global-states';

export default initState({
    userCurrentLocation: null,
    delete_todo_processing: false,
    update_todo_modal: false,
})