import io from 'socket.io-client';
import AsyncStorage from '@react-native-async-storage/async-storage';
import feathers from '@feathersjs/feathers';
import socketio from '@feathersjs/socketio-client';
import authentication from '@feathersjs/authentication-client';

let app = null;

export const Connect = (BACKEND_URL) => {
    const socket = io(BACKEND_URL, {
        transports: ['websocket'],
        forceNew: true
    });

    const client = feathers();

    client.configure(socketio(socket));
    client.configure(authentication({
        storage: AsyncStorage
    }));

    app = client;
}

export const Service = (serviceName) => {
    if(app) {
        return app.service(serviceName);
    }
  
    return null
};

export const Props = () => {
    if(app) return app;
  
    return null;
}

export const isConnected = () => {
   return app.io.connected;
}