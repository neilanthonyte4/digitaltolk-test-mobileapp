import React from 'react';
import {View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Theme from './theme.json';
import Pages from './pages';
import HeaderButton from './lib/HeaderButtons';

const Stack = createNativeStackNavigator();

const headerRightLogoutBtn = (props) => {
    return (
        <View style={{flexDirection: 'row'}}>
            <HeaderButton.HeaderLogoutButton {...props} />
        </View>
    )
}

const headerOptions = (params) => {
    const {headerShown = true} = params;

    return {
        headerStyle: {
            backgroundColor: Theme.primary,
        },
        headerTintColor: Theme.secondary,
        headerTitleStyle: {
            fontWeight: '800',
            color: Theme.secondary
        },
        headerShown,
        headerBackTitleVisible: false,
        headerRight: () => params && params.headerRight ? params.headerRight(params) : null,
    }
}

const Navs = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="LOGIN" component={Pages.Login} options={(props) => headerOptions({...props, ...{headerShown: false}})} />
                <Stack.Screen name="NEW TODO" component={Pages.NewTodo} options={(props) => headerOptions(props)} />
                <Stack.Screen name="MY TODOS" component={Pages.Dashboard} options={(props) => headerOptions({...props, ...{headerRight: headerRightLogoutBtn}})} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default Navs;