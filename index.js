/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './src';
import {name as appName} from './app.json';
import * as Backend from './src/db_con';
import './src/globalStates';

Backend.Connect('https://my-feathers-backend.herokuapp.com');
// Backend.Connect('http://localhost:3030');

AppRegistry.registerComponent(appName, () => App);
